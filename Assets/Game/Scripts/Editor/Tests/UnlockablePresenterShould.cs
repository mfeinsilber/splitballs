﻿using System.Collections.Generic;
using Game.Scripts.Domain.Presentation;
using Moq;
using NUnit.Framework;

namespace Game.Scripts.Editor.Tests
{
    [TestFixture]
    public class UnlockablePresenterShould
    {
        private const int UNLOCK_AMOUNT = 2;
        
        private StubUnlockablePresenter _presenter;
        private Mock<IUnlockableView> _view;
        private Mock<IEntity> _target;

        [SetUp]
        public void Setup()
        {
            _view = new Mock<IUnlockableView>();
            _target = new Mock<IEntity>();
            _presenter = new StubUnlockablePresenter(_view.Object, UNLOCK_AMOUNT);
        }
        
        [Test]
        public void Decrement_Needed_Amount_On_Trigger()
        {
            WhenTriggering();
            ThenUnlockAmountIsReduced();
        }

        [Test]
        public void Disable_Ball_On_Trigger()
        {
            _presenter.OnTriggerFor(_target.Object);
            _view.Verify(view => view.DisableBall(_target.Object));
        }

        [Test]
        public void Store_Entity_On_Trigger()
        {
            _presenter.OnTriggerFor(_target.Object);
            Assert.That(_presenter.Entities, Has.Member(_target.Object));
        }

        private void WhenTriggering()
        {
            _presenter.OnTriggerFor(_target.Object);
        }

        private void ThenUnlockAmountIsReduced()
        {
            Assert.AreEqual(UNLOCK_AMOUNT - 1, _presenter.UnlockAmount);
        }
    }

    public class StubUnlockablePresenter : UnlockablePresenter
    {
        public List<IEntity> Entities => _entities;
        public int UnlockAmount => _unlockAmount;

        public StubUnlockablePresenter(IUnlockableView view, int unlockAmount) : base(view, unlockAmount)
        {
        }
    }
}
