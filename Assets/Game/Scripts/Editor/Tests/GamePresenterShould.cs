﻿using Game.Scripts.Domain.Presentation;
using Moq;
using NUnit.Framework;
using UnityEngine;

namespace Game.Scripts.Editor.Tests
{
    [TestFixture]
    public class GamePresenterShould
    {
        private Mock<IGameView> _view;
        private GamePresenter _presenter;

        [SetUp]
        public void Setup()
        {
            _view = new Mock<IGameView>();
            _presenter = new GamePresenter(_view.Object);
        }
        
        [Test]
        public void Rotate_Level_On_Swipe()
        {
            WhenSwiping();
            ThenLevelIsRotated();
        }

        private void WhenSwiping()
        {
            _presenter.OnSwipe(It.IsAny<Vector3>());
        }

        private void ThenLevelIsRotated()
        {
            _view.Verify(view => view.SetLevelRotation(It.IsAny<float>()), Times.Once());
        }
    }
}
