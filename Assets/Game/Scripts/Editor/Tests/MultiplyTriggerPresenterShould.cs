﻿using System.Collections;
using System.Collections.Generic;
using Game.Scripts.Domain.Presentation;
using Moq;
using NUnit.Framework;

namespace Game.Scripts.Editor.Tests
{
    [TestFixture]
    public class MultiplyTriggerPresenterShould
    {
        private const int ENTITY_ID = 1;
        
        private StubMultiplyTriggerPresenter _presenter;
        private Mock<IMultiplyTriggerView> _view;
        private Mock<IEntity> _target;

        [SetUp]
        public void Setup()
        {
            _view = new Mock<IMultiplyTriggerView>();
            _target = new Mock<IEntity>();
            _presenter = new StubMultiplyTriggerPresenter(_view.Object);

            _target.Setup(t => t.Id).Returns(ENTITY_ID);
        }

        [Test]
        public void Multiply_Entity_On_Trigger()
        {
            WhenTriggeringWith(_target.Object);
            ThenEntityIsMultiplied();
        }

        [Test]
        public void Save_Entity_Id_On_Trigger()
        {
            WhenTriggeringWith(_target.Object);
            ThenEntityIdIsStored();
        }

        [Test]
        public void Prevent_Multiply_If_Id_Already_Multiplied()
        {
            GivenAnAlreadyMultipliedEntity();
            WhenTriggeringWith(_target.Object);
            ThenEntityIsNotMultiplied();
        }

        [Test]
        public void Store_Clone_Entity_Id()
        {
            WhenSavingEntityId();
            ThenEntityIdIsStored();
        }

        private void GivenAnAlreadyMultipliedEntity()
        {
            _presenter.SetId(ENTITY_ID);
        }

        private void WhenSavingEntityId()
        {
            _presenter.SaveEntityId(_target.Object);
        }

        private void WhenTriggeringWith(IEntity target)
        {
            _presenter.OnTriggerFor(target);
        }

        private void ThenEntityIsMultiplied()
        {
            _view.Verify(view => view.MultiplyObject(), Times.Once());
        }
        
        private void ThenEntityIsNotMultiplied()
        {
            _view.Verify(view => view.MultiplyObject(), Times.Never());
        }

        private void ThenEntityIdIsStored()
        {
            Assert.That(_presenter.EntityIds, Has.Member(ENTITY_ID));
        }

        private class StubMultiplyTriggerPresenter : MultiplyTriggerPresenter
        {
            public HashSet<int> EntityIds => _entityIds;

            public StubMultiplyTriggerPresenter(IMultiplyTriggerView view) : base(view)
            {
                
            }

            public void SetId(int entityId)
            {
                _entityIds.Add(entityId);
            }

            
        }
    }

    
}