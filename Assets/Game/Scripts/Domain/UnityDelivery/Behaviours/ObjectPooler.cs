﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Domain.Model;
using Game.Scripts.Domain.Presentation;
using Game.Scripts.Domain.UnityDelivery.Views;

public class ObjectPooler : MonoBehaviour {

	public static ObjectPooler Instance;
  [SerializeField] private List<ObjectPoolItem> itemsToPool;
  
  private List<EntityView> _pooledObjects;

	void Awake() {
    Instance = this;
    
    _pooledObjects = new List<EntityView>();
    foreach (ObjectPoolItem item in itemsToPool) {
      for (int i = 0; i < item.amountToPool; i++) {
        var obj = Instantiate(item.objectToPool, transform);
        obj.Init(i + 1);
        obj.gameObject.SetActive(false);
        _pooledObjects.Add(obj);
      }
    }
	}

  public EntityView GetPooledObject(int Id)
  {
    var entity = _pooledObjects.FirstOrDefault(x => x.Id.Equals(Id));

    return entity != null ? GetPooledObject(entity.tag) : null;
  }
	
  public EntityView GetPooledObject(string tag) {
    for (int i = 0; i < _pooledObjects.Count; i++) {
      if (!_pooledObjects[i].gameObject.activeInHierarchy && _pooledObjects[i].CompareTag(tag)) {
        return _pooledObjects[i];
      }

    }
    foreach (ObjectPoolItem item in itemsToPool) {
      if (item.objectToPool.CompareTag(tag)) {
        if (item.shouldExpand) {
          var obj = Instantiate(item.objectToPool, transform);
          obj.gameObject.SetActive(false);
          _pooledObjects.Add(obj);
          return obj;
        }
      }
    }
    return null;
  }
}
