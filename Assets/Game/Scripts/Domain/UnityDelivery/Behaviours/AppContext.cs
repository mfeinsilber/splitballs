﻿using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.Views
{
    public class AppContext : MonoBehaviour
    {
        [SerializeField] private GameView gameView;

        void Start()
        {
            gameView.Init();
        }
    }
}