﻿using Game.Scripts.Domain.Model;
using Game.Scripts.Domain.Presentation;
using Game.Scripts.Domain.UnityDelivery.ScriptableObjects;
using UniRx;
using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.Views
{
    public class GameView : MonoBehaviour, IGameView
    {
        [SerializeField] private GameSettings gameSettings;
        [SerializeField] private Transform level;
        [SerializeField] private Camera cam;
        [SerializeField] private int startBallAmount = 1;
        [SerializeField] private Transform spawnBallPosition;

        private GamePresenter _presenter;

        public void Init()
        {
            _presenter = new GamePresenter(this);
            
            InitMultiplyTriggers();
            InitUnlockableTriggers();

            HandleSwipe();

            SpawnBalls();
        }

        public void SetLevelRotation(float rotation)
        {
            RotateLevelBy(rotation);
            RotateAndMoveCameraTowards(rotation);
        }

        private void InitMultiplyTriggers()
        {
            var multiplyViews = GetComponentsInChildren<IMultiplyTriggerView>();
            foreach (var multiplyTriggerView in multiplyViews)
            {
                multiplyTriggerView.Init();
            }
        }

        private void InitUnlockableTriggers()
        {
            var unlockableViews = GetComponentsInChildren<IUnlockableView>();
            foreach (var unlockableView in unlockableViews)
            {
                unlockableView.Init();
            }
        }

        private void SpawnBalls()
        {
            for (int i = 0; i < startBallAmount; i++)
            {
                var ball = ObjectPooler.Instance.GetPooledObject(PooledObjects.Ball.ToString());
                ball.transform.SetParent(spawnBallPosition);
                ball.transform.position = spawnBallPosition.position;
                ball.gameObject.SetActive(transform);
            }
        }

        private void HandleSwipe()
        {
            Observable.EveryFixedUpdate().Do(_ =>
            {
                if (Input.GetMouseButton(0))
                {
                    _presenter.OnSwipe(Input.mousePosition);
                }
            }).Subscribe();
        }

        private void RotateLevelBy(float rotation)
        {
            Physics.gravity = new Vector3(
                Mathf.Lerp(gameSettings.LeftGravity, gameSettings.RightGravity, (1f + rotation) / 2f), 
                Physics.gravity.y,
                0f);
        }

        private void RotateAndMoveCameraTowards(float rotation)
        {
            
            cam.transform.localEulerAngles = new Vector3(0f,
                Mathf.Lerp(-gameSettings.MinCameraRotation, -gameSettings.MaxCameraRotation, (1f + rotation) / 2f), 0f);

            cam.transform.localPosition =
                new Vector3(Mathf.Lerp(gameSettings.MinCameraPan, gameSettings.MaxCameraPan, (1f + rotation) / 2f),
                    cam.transform.localPosition.y, cam.transform.localPosition.z);
        }
    }
}