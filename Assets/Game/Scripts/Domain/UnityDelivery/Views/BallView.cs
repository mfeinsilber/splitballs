﻿using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.Views
{
    public class BallView : EntityView
    {
        [SerializeField] private Rigidbody rigidBody;
        public void SetPosition(Vector3 position)
        {
            rigidBody.position = position;
        }

        public void StopRotation()
        {
            rigidBody.angularVelocity = Vector3.zero;;
        }
    }
}
