﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Domain.Presentation;
using UniRx;
using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.Views
{
    public class UnlockableView : MonoBehaviour, IUnlockableView
    {
        [SerializeField] private int unlockAmount;

        private UnlockablePresenter _presenter;

        public void Init()
        {
            _presenter = new UnlockablePresenter(this, unlockAmount);
        }

        private void OnTriggerEnter(Collider other)
        {
            var entity = other.GetComponent<IEntity>();

            if (entity == null) return;

            _presenter.OnTriggerFor(entity);
        }

        public void DisableBall(IEntity target)
        {
            var entity = (EntityView) target;
            entity.gameObject.SetActive(false);
        }

        public void EnableBalls(List<IEntity> entities)
        {
            var balls = entities.Cast<BallView>();

            var sequence = new List<IObservable<Unit>>();

            foreach (var ball in balls)
            {
                sequence.Add(Observable.ReturnUnit().Do(_ =>
                {
                    ball.SetPosition(transform.position);
                    ball.StopRotation();
                    ball.gameObject.SetActive(true);
                }).Delay(TimeSpan.FromMilliseconds(100f)));
            }

            sequence.Concat().Subscribe();
        }

        public void Destroy()
        {
            gameObject.SetActive(false);
        }
    }
}