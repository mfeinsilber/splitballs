﻿using System;
using Game.Scripts.Domain.Model;
using Game.Scripts.Domain.Presentation;
using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.Views
{
    public class MultiplyTriggerView : MonoBehaviour, IMultiplyTriggerView
    {
        [SerializeField] private PooledObjects target;
        [SerializeField] private int multiplier;
        [SerializeField] private Transform spawnPosition;

        private MultiplyTriggerPresenter _presenter;


        public void Init()
        {
            _presenter = new MultiplyTriggerPresenter(this);    
        }

        private void OnTriggerExit(Collider other)
        {
            var entity = other.GetComponent<IEntity>();

            if (entity == null) return;
            
            _presenter.OnTriggerFor(entity);
        }

        public void MultiplyObject()
        {
            for (int i = 1; i < multiplier; i++)
            {
                var item = ObjectPooler.Instance.GetPooledObject(target.ToString());
                _presenter.SaveEntityId(item);
                item.transform.position = spawnPosition.position;
                item.gameObject.SetActive(true);
            }
        }
    }
}
