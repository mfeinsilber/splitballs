using Game.Scripts.Domain.Presentation;
using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.Views
{
    public class EntityView : MonoBehaviour, IEntity
    {
        public int Id { get; private set; }

        public void Init(int id)
        {
            this.Id = id;
        }
    }
}