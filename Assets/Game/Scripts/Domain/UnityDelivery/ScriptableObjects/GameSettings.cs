﻿using UnityEngine;

namespace Game.Scripts.Domain.UnityDelivery.ScriptableObjects
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "GameSettings")]
    public class GameSettings : ScriptableObject
    {
        [SerializeField]private float leftGravity;
        public float LeftGravity => leftGravity;

        [SerializeField]private float rightGravity;
        public float RightGravity => rightGravity;

        [SerializeField]private float minCameraPan;
        public float MinCameraPan => minCameraPan;

        [SerializeField]private float maxCameraPan;
        public float MaxCameraPan => maxCameraPan;

        [SerializeField]private float minCameraRotation;
        public float MinCameraRotation => minCameraRotation;

        [SerializeField]private float maxCameraRotation;
        public float MaxCameraRotation => maxCameraRotation;

    }
}
