using Game.Scripts.Domain.UnityDelivery.Views;

namespace Game.Scripts.Domain.Model
{
    [System.Serializable]

    public class ObjectPoolItem {
        public EntityView objectToPool;
        public int amountToPool;
        public bool shouldExpand;
    }
}