using UnityEngine;

namespace Game.Scripts.Domain.Presentation
{
    public interface IGameView
    {
        void SetLevelRotation(float rotation);
    }
}