using System.Collections.Generic;

namespace Game.Scripts.Domain.Presentation
{
    public class MultiplyTriggerPresenter
    {
        private readonly IMultiplyTriggerView _view;
        protected HashSet<int> _entityIds;


        public MultiplyTriggerPresenter(IMultiplyTriggerView view)
        {
            _view = view;
            _entityIds = new HashSet<int>();
        }

        public void OnTriggerFor(IEntity target)
        {
            if (_entityIds.Contains(target.Id)) return;
            
            SaveEntityId(target);

            _view.MultiplyObject();
        }
        
        public void SaveEntityId(IEntity entity)
        {
            _entityIds.Add(entity.Id);
        }
    }
}