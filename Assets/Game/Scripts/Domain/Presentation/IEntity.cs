namespace Game.Scripts.Domain.Presentation
{
    public interface IEntity
    {
        int Id { get; }
        void Init(int id);
    }
}