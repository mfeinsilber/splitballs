using UnityEngine;

namespace Game.Scripts.Domain.Presentation
{
    public class GamePresenter
    {
        private readonly IGameView _view;

        private Vector3 _prevInputPosition = Vector3.zero;
        private float _rotation;
        
        public GamePresenter(IGameView view)
        {
            _view = view;
        }

        public void OnSwipe(Vector3 inputPosition)
        {
            CalculateRotationFrom(inputPosition);

            _view.SetLevelRotation(_rotation * -1);
        }

        private void CalculateRotationFrom(Vector3 inputPosition)
        {
            var deltaPosition = inputPosition - _prevInputPosition;
            _rotation += deltaPosition.x * 0.01f;
            _rotation = Mathf.Clamp(_rotation, -1f, 1f);
            
            _prevInputPosition = inputPosition;

        }
    }
}