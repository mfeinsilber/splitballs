using System.Collections.Generic;

namespace Game.Scripts.Domain.Presentation
{
    public interface IUnlockableView
    {
        void Init();
        void DisableBall(IEntity target);
        void EnableBalls(List<IEntity> entities);
        void Destroy();
    }
}