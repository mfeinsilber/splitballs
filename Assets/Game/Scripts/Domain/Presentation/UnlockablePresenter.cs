using System.Collections.Generic;

namespace Game.Scripts.Domain.Presentation
{
    public class UnlockablePresenter
    {
        private readonly IUnlockableView _view;
        protected int _unlockAmount;
        protected List<IEntity> _entities;
        public UnlockablePresenter(IUnlockableView view, int unlockAmount)
        {
            _view = view;
            _unlockAmount = unlockAmount;
            _entities = new List<IEntity>();
        }
        
        public void OnTriggerFor(IEntity target)
        {
            _unlockAmount--;
            _view.DisableBall(target);
            _entities.Add(target);

            if (_unlockAmount <= 0)
            {
                _view.EnableBalls(_entities);
                _view.Destroy();
            }
        }
    }
}