namespace Game.Scripts.Domain.Presentation
{
    public interface IMultiplyTriggerView
    {
        void Init();
        void MultiplyObject();
    }
}